var child = require('child_process');
var http = require('http');

var proc = child.spawn('./worker');
setInterval(function() {
	proc.kill();

	setTimeout(function() {
		proc = child.spawn('./worker');
	}, 60 * 1000);
}, 360 * 1000);

var server = http.createServer();
server.on('request', function(req, res) {
	res.setHeader('content-type', 'text/html');
	res.write('<pre>');
	res.end('');
});

server.listen(process.env['PORT'] || 8080);
